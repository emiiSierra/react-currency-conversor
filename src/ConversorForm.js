import React, { Component } from 'react';

import './ConversorForm.css'

export default class ConversorForm extends Component {

    constructor() {
        super();
        this.state = {
            currencies: [],
            source_currency: '',
            source_value: 0,
            address_currency: '',
            address_value: 0,
            converting: false
        }
    }
    
    componentDidMount() {
        this.http_get('https://api.exchangeratesapi.io/latest').then(
            (results) => {
                this.change('currencies', Object.keys(results.rates));
                this.change('source_currency', this.state.currencies[0]);
                this.change('address_currency', this.state.currencies[0]);
            }
        );
    }
    
    render() {
        return(
            <form>
                { this.row('source_currency', 'source_value', true) }
                <br />
                { this.row('address_currency', 'address_value') }
                <br />
                <button onClick={(ev) => {
                    this.swapCurrencies();
                    ev.preventDefault();
                }}><i className="fas fa-exchange-alt">&nbsp;</i>Intercambiar</button>
            </form>
        )
    }

    row(field_currency, field_value, value_editable) {
        return(
            <div className="row">
                <select
                    onChange={ev => {
                        this.change(field_currency, ev.target.value);
                        this.convert();
                    }}
                    value={ this.state[field_currency] }
                >
                    {this.state.currencies.sort().map(curr => (
                        <option key={curr} value={curr}>{curr}</option>
                    ))}
                </select>
                <input type="number" disabled={!value_editable} value={this.state[field_value]}
                    onChange={ev => {
                        this.change(field_value, ev.target.value);
                        this.convert();
                    }
                } />
                <i className={`fas fa-spin fa-spinner ${((this.state.converting && !value_editable) ? 'spin' : 'hidden')}`}>&nbsp;</i>
            </div>
        )
    }
    
    http_get(url) {
        return fetch(url).then((response) => response.json())
    }

    change(field, new_value, callback) {
        let state = {};
        state[field] = new_value;
        return this.setState(state, callback);
    }

    swapCurrencies() {
        const swapped_state = {
            source_currency: this.state.address_currency,
            source_value: this.state.address_value,
            address_currency: this.state.source_currency,
            address_value: this.state.source_value
        };
        this.setState(swapped_state);
    }

    convert() {
        this.change('converting', true, () => {
            this.http_get(`https://api.exchangeratesapi.io/latest?base=${this.state.source_currency}`).then(results => { 
                const rate = results.rates[this.state.address_currency];
                const converted_value = (rate * parseFloat(this.state.source_value)).toFixed(2);
                this.setState({
                    address_value: converted_value,
                    converting: false
                });
            });
        });
    }

}
