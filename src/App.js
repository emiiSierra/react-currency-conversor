import React, { Component } from 'react';
import ConversorForm from './ConversorForm';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <ConversorForm />
      </div>
    );
  }
}

export default App;
